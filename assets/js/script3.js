const Toast = Swal.mixin({
	toast : true,
	position : 'top-end',
	showConfirmButton : false,
	timer : 3000
})

document.querySelector("#startDiagnostic").addEventListener("click", () => {

	let uName = document.querySelector("#uName").value;
	let email = document.querySelector("#email").value;
	

	
	let errors = 0;


	//first name should not have numeric characters and the field should not be empty
	if (uName.length < 8) {

		document.querySelector("#uNameErr").innerHTML = `<small class="alert-danger">Username should be atleast 8 characters</small>`;
		errors++;
	}
	else {

		document.querySelector("#uNameErr").innerHTML = `<small class="alert-success">Username should be atleast 8 characters</small>`;
	}

	//email
	if (email.includes("@") && email.includes("mail.com")) {
		document.querySelector("#emailErr").innerHTML = `<small class="alert-success">Email is valid!</small>`;

	}
	else {
		document.querySelector("#emailErr").innerHTML = `<small class="alert-danger">Enter a valid email</small>`;
		errors ++;
	}

	//empty fields
	if(uName == "" || email == "") {
		errors++;
	}


	//alert message
	if(errors > 0) {
		Toast.fire({
			type : 'error',
			title : 'Something went wrong!'
		})
	}
	else {
		Toast.fire({
			type : 'success',
			title : "Let's check your status!"
		})
		localStorage.setItem("username", uName);
		localStorage.setItem("email", email);
		//if the sign in is successful
		window.location.href="./q1.html"
	}


})