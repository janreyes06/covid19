let username = localStorage.getItem("username");
let email = localStorage.getItem("email");
let q1 = localStorage.getItem("Traveller");

let q2 = localStorage.getItem("q2");
let q3 = localStorage.getItem("q3");

document.querySelector("#profileName").innerHTML = "Hello, " + username;

if (q1 == "A traveller" && q2 == "yes" && q3 == "yes") {
	document.querySelector("#status").innerHTML = "You are a PUI (Person Under Investigation)"
	document.querySelector("#status").style.color = "red"

	document.querySelector("#r1").innerHTML = "You have travelled to countries that have COVID-19 transmissions in the past 14 days."
	document.querySelector("#r2").innerHTML = "You have been with a COVID-19 patient."
	document.querySelector("#r3").innerHTML = "You have been suffering some symptoms of COVID-19."

	document.querySelector("#step1").innerHTML = "1. You have to go to the hospital RIGHT NOW!"
	document.querySelector("#step1").style.color = "red"
	document.querySelector("#step2").innerHTML = "2. Contact this number DOH: 02-894-26843"
	document.querySelector("#step2").style.color = "red"

}
else if (q1 == "A traveller" && q2 == "yes" && q3 == "no") {
	document.querySelector("#status").innerHTML = "You are a PUM (Person Under Monitoring)"
	document.querySelector("#status").style.color = "blue"

	document.querySelector("#r1").innerHTML = "You have travelled to countries that have COVID-19 transmissions in the past 14 days."
	document.querySelector("#r2").innerHTML = "You have been with a COVID-19 patient."
	document.querySelector("#r3").innerHTML = "You haven't been suffering some symptoms of COVID-19."

	document.querySelector("#step1").innerHTML = "1. You have to undergo self-quarantine. This means that you have to stay at your home for 14 days."
	document.querySelector("#step2").innerHTML = "2. Maintain social distancing."
	document.querySelector("#step3").innerHTML = "3. Watch if symptoms may occur!"

}
else if (q1 == "A traveller" && q2 == "no" && q3 == "no") {
	document.querySelector("#status").innerHTML = "You are a PUM (Person Under Monitoring)"
	document.querySelector("#status").style.color = "blue"

	document.querySelector("#r1").innerHTML = "You have travelled to countries that have COVID-19 transmissions in the past 14 days."
	document.querySelector("#r2").innerHTML = "You haven't been with a COVID-19 patient."
	document.querySelector("#r3").innerHTML = "You haven't been suffering some symptoms of COVID-19."

	document.querySelector("#step1").innerHTML = "1. You have to undergo self-quarantine. This means that you have to stay at your home for 14 days."
	document.querySelector("#step2").innerHTML = "2. Maintain social distancing."
	document.querySelector("#step3").innerHTML = "3. Watch if symptoms may occur!"

}
else if (q1 == "A traveller" && q2 == "no" && q3 == "yes") {
	document.querySelector("#status").innerHTML = "You are a PUI (Person Under Investigation)"
	document.querySelector("#status").style.color = "red"

	document.querySelector("#r1").innerHTML = "You have travelled to countries that have COVID-19 transmissions in the past 14 days."
	document.querySelector("#r2").innerHTML = "You haven't been with a COVID-19 patient."
	document.querySelector("#r3").innerHTML = "You have been suffering some symptoms of COVID-19."

	document.querySelector("#step1").innerHTML = "1. You have to go to the hospital RIGHT NOW!"
	document.querySelector("#step1").style.color = "red"
	document.querySelector("#step2").innerHTML = "2. Contact this number DOH: 02-894-26843"
	document.querySelector("#step2").style.color = "red"

}
else if (q1 == "Not a traveller" && q2 == "yes" && q3 == "yes") {
	document.querySelector("#status").innerHTML = "You are a PUI (Person Under Investigation)"
	document.querySelector("#status").style.color = "red"

	document.querySelector("#r1").innerHTML = "You haven't travelled to countries that have COVID-19 transmissions in the past 14 days."
	document.querySelector("#r2").innerHTML = "You have been with a COVID-19 patient."
	document.querySelector("#r3").innerHTML = "You have been suffering some symptoms of COVID-19."

	document.querySelector("#step1").innerHTML = "1. You have to go to the hospital RIGHT NOW!"
	document.querySelector("#step1").style.color = "red"
	document.querySelector("#step2").innerHTML = "2. Contact this number DOH: 02-894-26843"
	document.querySelector("#step2").style.color = "red"
}
else if (q1 == "Not a traveller" && q2 == "yes" && q3 == "no") {
	document.querySelector("#status").innerHTML = "You are a PUM (Person Under Monitoring)"
	document.querySelector("#status").style.color = "blue"

	document.querySelector("#r1").innerHTML = "You haven't travelled to countries that have COVID-19 transmissions in the past 14 days."
	document.querySelector("#r2").innerHTML = "You have been with a COVID-19 patient."
	document.querySelector("#r3").innerHTML = "You haven't been suffering some symptoms of COVID-19."

	document.querySelector("#step1").innerHTML = "1. You have to undergo self-quarantine. This means that you have to stay at your home for 14 days."
	document.querySelector("#step2").innerHTML = "2. Maintain social distancing."
	document.querySelector("#step3").innerHTML = "3. Watch if symptoms may occur!"
}
else if (q1 == "Not a traveller" && q2 == "no" && q3 == "no") {
	document.querySelector("#status").innerHTML = "You are not a PUI or PUM (you are COVID-19 free!)"
	document.querySelector("#status").style.color = "green"

	document.querySelector("#r1").innerHTML = "You haven't travelled to countries that have COVID-19 transmissions in the past 14 days."
	document.querySelector("#r2").innerHTML = "You haven't been with a COVID-19 patient."
	document.querySelector("#r3").innerHTML = "You haven't been suffering some symptoms of COVID-19."

	document.querySelector("#step1").innerHTML = "1. Maintain cleanliness."
	document.querySelector("#step2").innerHTML = "2. Maintain social distancing."
	document.querySelector("#step3").innerHTML = "3. Stay at Home!"
}
else if (q1 == "Not a traveller" && q2 == "no" && q3 == "yes") {
	document.querySelector("#status").innerHTML = "You are a PUM (Person Under Monitoring)"
	document.querySelector("#status").style.color = "blue"

	document.querySelector("#r1").innerHTML = "You haven't travelled to countries that have COVID-19 transmissions in the past 14 days."
	document.querySelector("#r2").innerHTML = "You haven't been with a COVID-19 patient."
	document.querySelector("#r3").innerHTML = "You have been suffering possible symptoms of COVID-19."

	document.querySelector("#step1").innerHTML = "1. You have to undergo self-quarantine. This means that you have to stay at your home for 14 days."
	document.querySelector("#step2").innerHTML = "2. Maintain social distancing."
	document.querySelector("#step3").innerHTML = "3. Watch if more symptoms may occur!"

}

document.querySelector("#homebtn").addEventListener("click", () => {
	localStorage.clear();
	window.location.href = "./index.html";
})